class CalibreServer < Formula

  # do365/do365
  url 'http://download.sf.net/do365/2019/calibre-users.sqlite'
  sha256 '658ef41618733635c5ac04078fc0b8d94a99287ea97e650d2165bdebc8cd4cb2'

  def install
      libexec.install "calibre-users.sqlite"
  end

  # 
  plist_options :manual => "calibre-server"

  def plist; <<~EOS
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
      <dict>
        <key>Label</key>
        <string>#{plist_name}</string>
        <key>RunAtLoad</key>
        <true/>
        <key>KeepAlive</key>
        <false/>
        <key>ProgramArguments</key>
        <array>
          <string>/usr/local/bin/calibre-server</string>
          <string>--port</string>
          <string>8026</string>
          <string>--log</string>
          <string>/usr/local/var/log/calibre-server.log</string>
          <string>--pidfile</string>
          <string>/usr/local/var/run/calibre-server.pid</string>
          <string>--userdb</string>
          <string>/usr/local/opt/calibre-server/libexec/calibre-users.sqlite</string>
          <string>--enable-auth</string>
          <string>#{ENV['HOME']}/datahub/calibre-server-books</string>
        </array>
      </dict>
    </plist>
  EOS
  end

end
