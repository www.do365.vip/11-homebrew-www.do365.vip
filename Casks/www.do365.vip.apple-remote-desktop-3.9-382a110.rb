# http://youtrack.do365.vip:8365/issue/do365-27
cask 'www.do365.vip.apple-remote-desktop-3.9-382a110' do
  version '3.9-382A110'
  sha256 '05e8a36324a9f924efa626bd40307b14dbd3b4da4609601b3053799b3a0fe33b'

  url 'http://download.sf.net/do365/2019/www.do365.vip.apple-remote-desktop-3.9-382A110.zip'
  name 'www.do365.vip.apple-remote-desktop-3.9-382a110'
  homepage 'http://youtrack.do365.vip:8365/issue/do365-27'

  app 'Remote Desktop.app'
end