cask 'do365-navicat-premium-12.0.24' do
  version '12.0.24'
  sha256 '630496c694c07cc651245acfd7d3cbdeaa4efdf8de1fb1a7191f4656d73018d7'

  url 'http://download.sf.net/do365/navicat-premium-12.0.24-TNT.zip'
  name 'do365-navicat-premium-12.0.24'
  homepage 'https://www.do365.vip/thread-148-1-1.html'

  app 'Navicat Premium.app'
end