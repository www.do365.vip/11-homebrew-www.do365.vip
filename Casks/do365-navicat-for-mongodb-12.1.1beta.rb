cask 'do365-navicat-for-mongodb-12.1.1beta' do
  version '12.1.1beta'
  sha256 'cc9eaf9cc5343a78a9797b64359c3774d84bca66b2a87caf6d48c8b85fb7cae8'

  url 'http://download.sf.net/do365/navicat-for-mongodb-12.1.1beta-TNT.zip'
  name 'do365-navicat-for-mongodb-12.1.1beta'
  homepage 'https://www.do365.vip/thread-147-1-1.html'

  app 'Navicat for MongoDB.app'
end