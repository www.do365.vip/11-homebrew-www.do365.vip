# http://youtrack.do365.vip:8365/issue/do365-27
cask 'www.do365.vip.monosnap-3.5.5' do
  version '3.5.5'
  sha256 '74ad4e8851d354e3b263e6866af3ec4827b661a686b662fa3195698aade7fb32'
  url 'http://download.sf.net/do365/2019/www.do365.vip.monosnap-3.5.5.zip'
  name 'www.do365.vip.monosnap-3.5.5'

  app 'Monosnap.app'
end