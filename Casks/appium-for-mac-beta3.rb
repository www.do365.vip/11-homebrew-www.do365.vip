cask 'appium-for-mac-beta3' do
  version 'beta3'
  sha256 '26b395005ef419f4fc70e9d4f1e41611b0e4f7992efabecc17cd0ff211912192'

  url 'http://download.sf.net/do365/2019/AppiumForMac.zip'
  name 'AppiumForMac'
  homepage 'https://github.com/appium/appium-for-mac'

  app 'AppiumForMac.app'
end