cask 'postgresapp' do
  version '2.2.3-9.5-9.6-10-11'
  sha256 'e29b27aaf7b8a2e0aac0a1229aede761c45897bd3fc6a88c889217ae40abda16'

  url 'https://github.com/PostgresApp/PostgresApp/releases/download/v2.2.3/Postgres-2.2.3-9.5-9.6-10-11.dmg'
  name 'postgresapp'

  app 'Postgres.app'
end