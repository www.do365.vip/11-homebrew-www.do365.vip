cask 'www.do365.vip.navicat-for-mongodb12.1.13-tnt' do
  version '12.1.13-TNT'
  sha256 'e6d935451b112532b6576ab917b61f1af1e331a6faff7f6a103f67367bb8e7df'

  url 'http://download.sf.net/do365/www.do365.vip.navicat-for-mongodb12.1.13-TNT.zip'
  name 'www.do365.vip.navicat-for-mongodb12.1.13-tnt'
  homepage 'https://www.do365.vip/thread-432-1-1.html'

  app 'Navicat for MongoDB.app'
end