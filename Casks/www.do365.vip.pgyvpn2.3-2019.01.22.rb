# http://youtrack.do365.vip:8365/issue/do365-27
cask 'www.do365.vip.pgyvpn2.3-2019.01.22' do
  version '2.3-2019.01.22'
  sha256 '92a50ad28fdcdbcde1d68e0f5ff31cd38408318da39af26ef83fff1584cb9f4b'

  url 'http://download.sf.net/do365/2019/www.do365.vip.pgyvpn2.3-2019.01.22.zip'
  name 'www.do365.vip.pgyvpn2.3-2019.01.22'
  homepage 'http://youtrack.do365.vip:8365/issue/do365-27'

  app '蒲公英.app'
end