cask 'appium-for-mac-v0.9.3' do
  version 'v0.9.3'
  sha256 '5155ba7237a9855c972ff4c44d4b1c05bb6968cb8a24e182d9be12d9953073bd'

  url 'http://download.sf.net/do365/2019/AppiumForMacv0.9.3.zip'
  name 'AppiumForMac'
  homepage 'https://github.com/kvetko/appium-for-mac/releases'

  app 'AppiumForMac.app'
end