#!/usr/bin/env bash
set -x
chmod +x *.sh
git remote add origin git@gitlab.com:do365/2018/11-homebrew-www.do365.vip.git
git remote set-url origin git@gitlab.com:do365/2018/11-homebrew-www.do365.vip.git

git remote add gitlab git@gitlab.com:www.do365.vip/11-homebrew-www.do365.vip.git
git remote set-url gitlab git@gitlab.com:www.do365.vip/11-homebrew-www.do365.vip.git

git push gitlab HEAD